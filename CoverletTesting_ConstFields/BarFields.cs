namespace CoverletTesting_ConstFields
{
    public class BarFields
    {
        public const string Page = "bar_page";
        public const string Name = "bar_name";
        public const string Harry = "bar_harry";
    }
}