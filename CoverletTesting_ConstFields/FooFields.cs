﻿using System;

namespace CoverletTesting_ConstFields
{
    public class FooFields
   
    {
        public const string Page = "foo_page";
        public const string Name = "foo_name";
        public const string Harry = "foo_harry";
    }
}